# lmgtfy-discord
Never get annoyed by __*a person who can't search google*__ again! - Let the bot Google that for you!

## How to install
Install [node](https://nodejs.org/), clone the repo, and run `npm i` inside the cloned folder.

Then run `node index.js` to start the bot!

## What is a bot token?
Well, google it!

You can also read [this good guide](https://anidiots.guide/getting-started/getting-started-long-version).

## How to use
When you run the bot, the bot will display the invitation link in the console output.

***IMPORTANT!***: The bot requires *manage messages* permission to function properly.

Say `search! help` (or `<prefix>! help`) to get help with *in-discord commands*.

## License
This bot (NOT including the modules) is licenced under LGPL-3.0.

See http://gnu.org for details.