const request = require('request-promise-native');
const { JSDOM } = require('jsdom');
const htmlToText = require('html-to-text');

const google = (query) =>
{
	return new Promise(async (resolve, reject) =>
	{
		let raw, dom;

		try { raw = await request(`https://www.google.com/search?q=${encodeURIComponent(query)}`); }
		catch(e) { reject(e); }

		try { dom = new JSDOM(raw); }
		catch(e) { reject(e); }

		const document = dom.window.document; 

		// Seems like topstuff is only being used by calculator. I might be wrong though.
		if(document.getElementById('topstuff').childNodes.length) parseCalculator(document, resolve);

		// All other quickaswers are in the first element with 'g' class, wrapped by 'div' tag
		else if(document.getElementsByClassName('g').item(0).childNodes.item(0).nodeName !== 'DIV') resolve({ type: null, answer: null });

		// had to hardcode patterns in this part (yikes!)
		else if(document.getElementsByClassName('g').item(0).getElementsByTagName('a').length === 0)
		{
			if(document.getElementsByClassName('g').item(0).childNodes.item(0).getAttribute('class') === 'g') parsePerson(document, resolve);
			else if(document.getElementsByClassName('g').item(0).getElementsByClassName('r').length !== 0) parseDefinition(document, resolve);
			else resolve({type: null, answer: null});
		}

		else if(document.getElementsByClassName('g').item(0).getElementsByTagName('a').item(0).getAttribute('href').includes('googlefinance/disclaim')) parseExchange(document, resolve);
		else if(document.getElementsByClassName('g').item(0).getElementsByTagName('a').item(0).getAttribute('href').includes('youtube.com/watch')) resolve({ type: null, answer: null });
		else if(document.getElementsByClassName('g').item(0).getElementsByTagName('a').item(0).getAttribute('href').includes('translate.google.com')) parseTranslation(document, resolve);
		else if(document.getElementsByClassName('g').item(0).getElementsByTagName('a').item(0).getAttribute('href').includes('/setprefs?')) resolve({ type: 'weather', answer: null });
		else if(document.getElementsByClassName('g').item(0).getElementsByTagName('a').item(0).getAttribute('href').includes('/url?q=')) parseLink(document, resolve);
		else resolve({ type: null, answer: null });
	});
};

const parseCalculator = async (document, cb) =>
{
	const obj = 
	{
		type: 'calculator',
		answer: document.getElementById('topstuff').getElementsByClassName('r').item(0).textContent
	};
	cb(obj);
};

const parsePerson = async (document, cb) =>
{
	const obj = 
	{
		type: 'person',
		answer: 
			{
				name: document.getElementsByClassName('g').item(0).getElementsByTagName('span').item(0).textContent,
				profession: document.getElementsByClassName('g').item(0).getElementsByTagName('span').item(0).nextSibling.textContent.trim()
			}
	};
	cb(obj);
};

const parseDefinition = async (document, cb) =>
{
	const obj = 
	{
		type: 'definition',
		answer:
			{
				name: document.getElementsByClassName('g').item(0).getElementsByTagName('span').item(0).textContent,
				pronunciation: document.getElementsByClassName('g').item(0).getElementsByTagName('span').item(1).textContent,
				partofspeech: document.getElementsByClassName('g').item(0).querySelector('[style="color:#666;padding:5px 0"]').textContent,
				definition: document.getElementsByClassName('g').item(0).getElementsByTagName('li').item(0).textContent
			}
	};
	cb(obj);
};

const parseExchange = async (document, cb) =>
{
	const obj = 
	{
		type: 'exchange',
		answer: document.getElementsByClassName('g').item(0).getElementsByTagName('a').item(0).previousSibling.textContent
	};
	cb(obj);
};

const parseTranslation = async (document, cb) =>
{
	const obj =
	{
		type: 'translation',
		answer: document.getElementsByClassName('g').item(0).getElementsByTagName('span').item(1).textContent
	};
	cb(obj);
};

const parseLink = async (document, cb) =>
{
	const obj = 
	{
		type: 'link',
		image: document.getElementsByClassName('g').item(0).getElementsByTagName('img').length !== 0 ? document.getElementsByClassName('g').item(0).getElementsByTagName('img').item(0).getAttribute('src') : null,
		linkname: await htmlToText.fromString(document.getElementsByClassName('g').item(0).getElementsByClassName('r').item(0).childNodes.item(0).innerHTML),
		link: 'https://google.com' + document.getElementsByClassName('g').item(0).getElementsByTagName('a').item(0).getAttribute('href'),
		answer: await htmlToText.fromString(document.getElementsByClassName('g').item(0).getElementsByClassName('r').item(0).parentNode.previousSibling.innerHTML.replace(/<a(.|\n)*?<\/a>/g, ''))
	};
	cb(obj);
};

module.exports = google;