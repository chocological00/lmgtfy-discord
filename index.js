process.on('unhandledRejection', (reason, p) => 
{
	console.log('Unhandled Rejection at: ', p, 'reason:', reason);
	// application specific logging, throwing an error, or other logic here
});

// dependencies
const fs = require('fs');
const Discord = require('discord.js');
const client = new Discord.Client();
const google = require('google');
const readline = require('readline');
const rl = readline.createInterface({ input: process.stdin, output: process.stdout });
const quickanswer = require('./google-quickanswer');

// configure library settings
google.resultsPerPage = 25;

async function main()
{
	// read access token
	let settings = {};
	if(fs.existsSync('settings.json'))
		settings = JSON.parse(fs.readFileSync('settings.json'));
	else
	{
		console.log('Bot token is not set!\n');
		settings.token = await questionAsync('Please enter the discord bot token below:\n');
		settings.prefix = await questionAsync('\nIf you would like to change the bot prefix, please enter it below.\n' +
			'Otherwise, leave it blank and press enter.\n' +
			'The default is search:') || 'search';
		fs.writeFileSync('settings.json', JSON.stringify(settings));
	}
	
	// start the bot

	client.login(settings.token);
	client.on('ready', async () =>
	{
		console.log(`Logged in as ${client.user.tag}!\n`);
		console.log(`Invitation Link: https://discordapp.com/oauth2/authorize?&client_id=${client.user.id}&scope=bot&permissions=0\n`);
	});
	
	// when newly invited
	client.on('guildCreate', async (guild) =>
	{
		const defaultChannel = await getDefaultChannel(guild);
		const embed = new Discord.RichEmbed();
		embed.setTitle(`Hello, I'm ${client.user.username}!`);
		embed.setAuthor(client.user.username, client.user.avatarURL);
		embed.setColor(9936031);
		embed.setFooter(client.user.username, client.user.avatarURL);
		embed.setTimestamp();
		embed.setDescription(`My bot prefix is \`${settings.prefix}!\`.\n` +
			`Say \`${settings.prefix}! help\` to call help!`);
		defaultChannel.send({embed});
	});
	
	client.on('message', async (message) =>
	{
		if(chkMessage(message))
		{
			let content = message.content.substr(settings.prefix.length).trim();
			if(content.toUpperCase().startsWith('!'))
			{
				content = content.substr(1).trim();
				if(content.toUpperCase() === 'HELP')
				{
					const embed = new Discord.RichEmbed();
					embed.setTitle(`Bot Help`);
					embed.setAuthor(client.user.username, client.user.avatarURL);
					embed.setColor(9936031);
					embed.setFooter(client.user.username, client.user.avatarURL);
					embed.setTimestamp();
					embed.setDescription(`The bot prefix is \`${settings.prefix}\`! You can use following commands to search:`);
					embed.addField(`${settings.prefix}! <keyword>`, 'Search `keyword` from Google');
					message.channel.send({embed});
				}
				else
				{
					const quickresult = await quickanswer(content);
					if(quickresult.type === null)
					{	
						let searchResults = senitizeResults((await searchGoogle(content)).links);

						const page = 0;
						const embed = await createEmbed(content, searchResults, page);
						const botMsg = await message.channel.send(embed);
						await botMsg.react('➡');
						if(page != searchResults.length - 1) await botMsg.react('✅');
						const filter = (reaction, user) =>
						{
							if(user.id !== message.author.id) return false;
							if(reaction.emoji.toString() == '➡' || reaction.emoji.toString() == '✅') return true;
							return false;
						};
						const collector = botMsg.createReactionCollector(filter, { time: 15000 });
						collector.on('collect', async r => 
						{
							await collector.stop();
							if(r.emoji.toString() == '➡' && page != searchResults.length - 1) editResponse(botMsg, searchResults, page + 1);
						});
						collector.on('end', async () => await botMsg.clearReactions());

						const editResponse = async (sentMsg, results, page) =>
						{
							const embed = await createEmbed(content, results, page);
							await sentMsg.edit(embed);
							if(page != 0) await sentMsg.react('⬅');
							if(page != results.length - 1) await sentMsg.react('➡');
							await sentMsg.react('✅');
							const filter = (reaction, user) =>
							{
								if(user.id !== message.author.id) return false;
								if(reaction.emoji.toString() == '⬅' || reaction.emoji.toString() == '➡' || reaction.emoji.toString() == '✅') return true;
								return false;
							};
							const collector = sentMsg.createReactionCollector(filter, { time: 15000 });
							collector.on('collect', async r => 
							{
								await collector.stop();
								if(r.emoji.toString() == '⬅' && page != 0) editResponse(sentMsg, results, page - 1);
								else if(r.emoji.toString() == '➡' && page != results.length - 1) editResponse(sentMsg, results, page + 1);
							});
							collector.on('end', async () => await sentMsg.clearReactions());
						};
					}
					else
					{
						if(quickresult.type === 'weather') message.channel.send('t!weather');
						else
						{
							const response = await generateResponse(content, quickresult);
							message.channel.send(response);
						}
					}
				}
			}
			// else if(content.startsWith('@'))
			// {
			// 	content = content.substr(1).trim();
			// 	const engine = content.split(' ')[0];
			// 	content = content.substr(engine.length).trim();
			// 	let embed;
			// 	if(engine.toUpperCase() === 'G'||engine.toUpperCase() === 'GOOGLE')
			// 		embed = await embedGoogle(content);
			// 	else
			// 	{
			// 		embed = new Discord.RichEmbed();
			// 		embed.setTitle('Error');
			// 		embed.setAuthor(client.user.username, client.user.avatarURL);
			// 		embed.setColor(9936031);
			// 		embed.setFooter(client.user.username, client.user.avatarURL);
			// 		embed.setTimestamp();
			// 		embed.setDescription(`Search engine \`${engine}\` is not supported :(\nAvailable options are: \`Google\``);
			// 	}
			// 	message.channel.send({embed});
			// }
		}
	});

	const chkMessage = (message) =>
	{
		return message.content.toUpperCase().startsWith(settings.prefix.toUpperCase());
	};
}

// helper functions
const getDefaultChannel = async (guild) =>
{
	// get "original" default channel
	if (guild.channels.has(guild.id)) return guild.channels.get(guild.id);
	
	// Check for a "general" channel, which is often default chat
	if (guild.channels.exists("name", "general")) return guild.channels.find("name", "general");
	
	// first channel in order where the bot can speak
	return guild.channels
		.filter(c => c.type === "text" &&
			c.permissionsFor(guild.client.user).has("SEND_MESSAGES"))
		.sort((a, b) => a.position - b.position ||
			Long.fromString(a.id).sub(Long.fromString(b.id)).toNumber())
		.first();
};

const searchGoogle = async (query) =>
{
	return new Promise((resolve, reject) =>
	{
		google(query, (err, res) =>
		{
			if(err) reject(err);
			else resolve(res);
		});
	});
};

const questionAsync = (question) =>
{
	return new Promise((resolve) =>
	{
		rl.question(question, (answer) =>
		{
			resolve(answer);
		});
	});
};

const senitizeResults = (results) =>
{
	let rtn = [];
	for(let result in results)
	{
		if(results[result].link) rtn.push(results[result]);
	}
	return rtn;
};

const createEmbed = async (keyword, results, number) =>
{
	const embed = new Discord.RichEmbed();
	embed.setTitle(`Results for ${keyword} (${number + 1}/${results.length})`);
	embed.setAuthor('Google Search Result', 'http://www.google.com/s2/favicons?domain=www.google.com');
	embed.setColor(9936031);
	embed.setFooter(client.user.username, client.user.avatarURL);
	embed.setTimestamp();
	embed.setURL('http://google.com/search?q=' + encodeURIComponent(keyword));
	embed.addField(results[number].title, `${results[number].description||'No Description Available'}\n[More](${results[number].link.replace(/\)/g, '\\)')})`);
	return embed;
};

const generateResponse = async (keyword, quickresult) =>
{
	const embed = new Discord.RichEmbed();
	embed.setTitle(`Result for ${keyword}`);
	embed.setAuthor('Google Quick Answer', 'http://www.google.com/s2/favicons?domain=www.google.com');
	embed.setColor(9936031);
	embed.setFooter(client.user.username, client.user.avatarURL);
	embed.setTimestamp();
	embed.setURL('http://google.com/search?q=' + encodeURIComponent(keyword));
	switch(quickresult.type)
	{
	case 'calculator': case 'exchange': case 'translation': 
		embed.addField(quickresult.type.substr(0,1).toUpperCase() + quickresult.type.substr(1), quickresult.answer);
		break;

	case 'person':
		embed.addField(quickresult.answer.profession, quickresult.answer.name);
		break;

	case 'definition':
		embed.addField(quickresult.answer.name + ' *' + quickresult.answer.pronunciation + '*', '*' + quickresult.answer.partofspeech + '*\n' + quickresult.answer.definition);
		break;
	
	case 'link':
		embed.addField(quickresult.linkname, quickresult.answer + '\n[More](' + quickresult.link.replace(/\)/g, '\\)') + ')');
		if(quickresult.image) embed.setThumbnail(quickresult.image);
		break;
	}
	return embed;
};

main();